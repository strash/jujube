/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *                                                                                               *
 *  JUJUBE VER 3.0 (2017)                                                                        *
 *  Plugin for <img> with imitation style background-size:cover and background-position:center   *
 *                                                                                               *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *                                                                                               *
 *  Autor: Dmitry Poyarkov / strash.ru / Since 2014                                                    *
 *                                                                                               *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 */

// Add data-attribute to images `data-name="jujube"`
// Using example:
// <div class="mask">
//   <img data-name="jujube" src="img/image.jpg">
// </div>
//
// Parent block (mask): .mask {position: relative;}
// image: .musk img {position: absolute;}
//
// To activate:
// window.onload = function () {
//   jujube.gogogo();
// };


var JUJUBEIMG = document.querySelectorAll('img');
var jujube = {
  compare: function (a, b) {
    if (a > b) return 'hor';
    if (a < b) return 'ver';
    if (a == b) return 'sqr';
  },

  ratio: function (object, i) {
    if (this[object + 'Orient' + (i + 1)] == 'hor') return this[object + 'Width' + (i + 1)] / this[object + 'Height' + (i + 1)];
    if (this[object + 'Orient' + (i + 1)] == 'ver') return this[object + 'Height' + (i + 1)] / this[object + 'Width' + (i + 1)];
    if (this[object + 'Orient' + (i + 1)] == 'sqr') return 1;
  },

  resize: function (i) {
    var blockW, imgOr, imgR;
    blockW = this['blockWidth' + (i + 1)];
    imgOr = this['imgOrient' + (i + 1)];
    imgR = this['imgRatio' + (i + 1)];
    if (imgOr == 'hor') return blockW / imgR;
    if (imgOr == 'ver') return blockW * imgR;
    if (imgOr == 'sqr') return blockW;
  },

  setJujube: function (i) {
    var blockOr, blockR, blockW, blockH, imgOr, imgR, topOffset, leftOffset, imgNewHeight;
    blockOr = this['blockOrient' + (i + 1)];
    blockR = this['blockRatio' + (i + 1)];
    blockW = this['blockWidth' + (i + 1)];
    blockH = this['blockHeight' + (i + 1)];
    imgOr = this['imgOrient' + (i + 1)];
    imgR = this['imgRatio' + (i + 1)];
    imgNewHeight = this['imgNewHeight' + (i + 1)];

    if (blockOr == 'hor' && imgOr == 'hor' && blockH == imgNewHeight){ // 1. hor hor b == i
      JUJUBEIMG[i].setAttribute('style', 'width:100%; height:100%; left: 0; top: 0;');
      JUJUBEIMG[i].setAttribute('data-number', '1');
    } else if (blockOr == 'hor' && imgOr == 'sqr' && blockH < imgNewHeight){ // 2. hor sqr b < i
      topOffset = (blockW / imgR - blockH) / 2;
      JUJUBEIMG[i].setAttribute('style', 'width:100%; top: -' + topOffset + 'px; left: 0;');
      JUJUBEIMG[i].setAttribute('data-number', '2');
    } else if (blockOr == 'hor' && imgOr == 'ver' && blockH < imgNewHeight){ // 3. hor ver b < i
      topOffset = (blockW * imgR - blockH) / 2;
      JUJUBEIMG[i].setAttribute('style', 'width:100%; top: -' + topOffset + 'px; left: 0;');
      JUJUBEIMG[i].setAttribute('data-number', '3');
    } else if (blockOr == 'hor' && imgOr == 'hor' && blockH < imgNewHeight){ // 4. hor hor b < i
      topOffset = (blockW / imgR - blockH) / 2;
      JUJUBEIMG[i].setAttribute('style', 'width:100%; top: -' + topOffset + 'px; left: 0;');
      JUJUBEIMG[i].setAttribute('data-number', '4');
    } else if (blockOr == 'hor' && imgOr == 'hor' && blockH > imgNewHeight){ // 5. hor hor b > i
      leftOffset = (blockH * imgR - blockW) / 2;
      JUJUBEIMG[i].setAttribute('style', 'height:100%; left: -' + leftOffset + 'px; top: 0;');
      JUJUBEIMG[i].setAttribute('data-number', '5');
    } else if (blockOr == 'ver' && imgOr == 'ver' && blockH == imgNewHeight){ // 6. ver ver b == i
      JUJUBEIMG[i].setAttribute('style', 'width:100%; height:100%; left: 0; top: 0;');
      JUJUBEIMG[i].setAttribute('data-number', '6');
    } else if (blockOr == 'ver' && imgOr == 'sqr' && blockH > imgNewHeight){ // 7. ver sqr b > i
      leftOffset = (blockH / imgR - blockW) / 2;
      JUJUBEIMG[i].setAttribute('style', 'height:100%; left: -' + leftOffset + 'px; top: 0;');
      JUJUBEIMG[i].setAttribute('data-number', '7');
    } else if (blockOr == 'ver' && imgOr == 'hor' && blockH > imgNewHeight){ // 8. ver hor b > i
      leftOffset = (blockH * imgR - blockW) / 2;
      JUJUBEIMG[i].setAttribute('style', 'height:100%; left: -' + leftOffset + 'px; top: 0;');
      JUJUBEIMG[i].setAttribute('data-number', '8');
    } else if (blockOr == 'ver' && imgOr == 'ver' && blockH > imgNewHeight){ // 9. ver ver b > i
      leftOffset = (blockH / imgR - blockW) / 2;
      JUJUBEIMG[i].setAttribute('style', 'height:100%; left: -' + leftOffset + 'px; top: 0;');
      JUJUBEIMG[i].setAttribute('data-number', '9');
    } else if (blockOr == 'ver' && imgOr == 'ver' && blockH < imgNewHeight){ // 10. ver ver b < i
      topOffset = (blockW * imgR - blockH) / 2;
      JUJUBEIMG[i].setAttribute('style', 'width:100%; top: -' + topOffset + 'px;');
      JUJUBEIMG[i].setAttribute('data-number', '10');
    } else if (blockOr == 'sqr' && imgOr == 'sqr' && blockH == imgNewHeight){ // 11. sqr sqr b == i
      JUJUBEIMG[i].setAttribute('style', 'width:100%; height:100%; left: 0; top: 0;');
      JUJUBEIMG[i].setAttribute('data-number', '11');
    } else if (blockOr == 'sqr' && imgOr == 'hor' && blockH > imgNewHeight){ // 12. sqr hor b > i
      leftOffset = (blockW * imgR - blockW) / 2;
      JUJUBEIMG[i].setAttribute('style', 'height:100%; left: -' + leftOffset + 'px; top: 0;');
      JUJUBEIMG[i].setAttribute('data-number', '12');
    } else if (blockOr == 'sqr' && imgOr == 'ver' && blockH < imgNewHeight){ // 13. sqr ver b < i
      topOffset = (blockW * imgR - blockH) / 2;
      JUJUBEIMG[i].setAttribute('style', 'width:100%; top: -' + topOffset + 'px; left: 0;');
      JUJUBEIMG[i].setAttribute('data-number', '13');
    }
  },

  gogogo: function () {
    var leftOffset, topOffset;
    for (var i = 0; i < JUJUBEIMG.length; i++) {
      if (JUJUBEIMG[i].dataset.name == 'jujube') {
        this['blockWidth' + (i + 1)]  = parseFloat(getComputedStyle(JUJUBEIMG[i].parentNode, '').width);
        this['blockHeight' + (i + 1)] = parseFloat(getComputedStyle(JUJUBEIMG[i].parentNode, '').height);
        this['blockOrient' + (i + 1)] = this.compare(this['blockWidth' + (i + 1)], this['blockHeight' + (i + 1)]);
        this['blockRatio' + (i + 1)]  = this.ratio('block', i);
        this['imgWidth' + (i + 1)]    = JUJUBEIMG[i].naturalWidth;
        this['imgHeight' + (i + 1)]   = JUJUBEIMG[i].naturalHeight;
        this['imgOrient' + (i + 1)]   = this.compare(this['imgWidth' + (i + 1)], this['imgHeight' + (i + 1)]);
        this['imgRatio' + (i + 1)]  = this.ratio('img', i);
        this['imgNewHeight' + (i + 1)] = this.resize(i);
        this.setJujube(i);
      }
    }
  }

};